var gulp = require('gulp');
var sass = require('gulp-sass');
var sassGlob = require('gulp-sass-glob');
var watch = require('gulp-watch');
var shell = require('gulp-shell');
var notify = require('gulp-notify');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');
var uglify = require('gulp-uglify');
var fs = require("fs");
var runSequence = require('run-sequence');
var imagemin = require('gulp-imagemin');
var config = require("./example.config");
var sassLint = require('gulp-sass-lint');
var jshint = require('gulp-jshint');
var path = require('path');
var savefile = require('gulp-savefile');
var notifier = require('node-notifier');
var del = require('del');

/**
 * If extra-config/config.js exists, load that config for overriding certain values below.
 */
function loadConfig() {
  if (fs.existsSync(__dirname + "/./config.js")) {
    config = {};
    config = require("./config");
  }

  return config;
}

loadConfig();

gulp.task('sasslint', function() {
  return gulp.src('src/scss/**/*.scss')
    .pipe(sassLint({
      options: {
        formatter: 'table',
      },
      configFile: '.sass-lint.yml'
    }))
    .pipe(sassLint.format())
    .pipe(sassLint.failOnError())
});

function handleError(err) {
  notifier.notify({ title: 'Error', message: err.toString() });
  console.log(err.toString());
}

function handleSucces(mes) {
  console.log(mes.toString());
}

function rebuildDrupalCache(path) {
  console.log(config.commands.cache_clear);
  gulp.src('gulpfile.js')
    .pipe(shell([
      config.commands.cache_clear
    ]))
    .pipe(notify({
      title: "Caches cleared",
      message: "Drupal CSS/JS caches cleared.",
      onLast: true
    }))
}

/**
 * This task minifies javascript in the src/js folder and places them in the dist/js directory.
 */
function compressJS(path) {
  gulp.src(path)
    .pipe(sourcemaps.init())
    .pipe(uglify().on('error', handleError))
    .pipe(sourcemaps.write('./maps'))
    .pipe(gulp.dest('./dist/js'))
  console.log('compiled global js');
};

function lintJSFile(path) {
  gulp.src(path)
    .pipe(jshint())
    .pipe(jshint.reporter('default'))
  console.log('linted ' + path + ' js');
}

function compressJSFile(path) {
  gulp
    .src(path)
    .pipe(sourcemaps.init())
    .pipe(uglify().on('error', handleError))
    .pipe(sourcemaps.write('/maps'))
    .pipe(gulp.dest(path.split( '/' ).slice( 0, -1 ).join( '/' ) + '/dist'))
    .pipe(shell([
      'echo "JS: compiled ' + path + '!"'
    ]))
}

function compileAllComponentsSCSS() {
  gulp
    .src(['components/**/*.scss', '!components/**/_*.scss'])
    .pipe(savefile());
}

function compileAllComponentSCSS(componentDir) {
  gulp
    .src([componentDir + '/**/*.scss', '!' + componentDir + '/**/_*.scss'])
    .pipe(savefile());
}

function compileSassFile(path, stats) {
  gulp
    .src(path)
    .pipe(sourcemaps.init())
    .pipe(sassGlob())
    .pipe(sass({
      noCache: true,
      outputStyle: "expanded",
      lineNumbers: false,
      loadPath: './css/*',
      sourceMap: true
    }).on('error', handleError))
    .pipe(autoprefixer({browsers: ['last 2 versions', 'ie 9']}))
    .pipe(sourcemaps.write('./maps'))
    .pipe(gulp.dest(path.split( '/' ).slice( 0, -1 ).join( '/' ) + '/dist'))
    .pipe(shell([
      'echo "SCSS: compiled ' + path + '!"'
    ]))
}

function minifyImage(path) {
  gulp.src(path)
    .pipe(imagemin([
    	imagemin.svgo({
    		plugins: [
          {removeViewBox: false},
          {removeDimensions: true}
    		]
    	})
    ]))
    .pipe(gulp.dest(path.split( '/' ).slice( 0, -1 ).join( '/' ) + '/dist'))
}

/**
 * Defines the watcher task.
 */
gulp.task('watch', function() {
  // When a change happens in variables, save all the component-sass files, so they get recompiled.
  gulp.watch(['_sass-essentials/*.scss'])
    .on("change", function(path, stats) {
      compileAllComponentsSCSS();
    })
    .on("add", function(path, stats) {
      compileAllComponentsSCSS();
    });

  // When a js-change happens in components, generate the minified js.
  gulp.watch(['components/**/*.js','!components/**/dist/*.js'])
    .on("change", function(path, stats) {
      compressJSFile(path);
      lintJSFile(path);
    })
    .on("add", function(path, stats) {
      compressJSFile(path);
      lintJSFile(path);
    })
    .on("unlink", function(path, stats) {
      var directory = path.split( '/' ).slice( 0, -1 ).join( '/');
      var filename = path.split( '/' ).slice( -1 ).join( '/');
      del([directory + '/dist/' + filename]);
      del([directory + '/dist/maps/' + filename + '.map']);
    });

  gulp.watch(['components/**/*.jpg', 'components/**/*.png', 'components/**/*.gif', 'components/**/*.svg', '!components/**/dist/*.*'])
    .on('change', function(path, stats) {
      minifyImage(path);
    })
    .on('add', function(path, stats) {
      minifyImage(path);
    })
    .on('unlink', function(path, stats) {
      var directory = path.split( '/' ).slice( 0, -1 ).join( '/');
      var filename = path.split( '/' ).slice( -1 ).join( '/');
      del([directory + '/dist/' + filename]);
    });

  // When a change happens in sass components, generate the css.
  gulp.watch(['components/**/*.scss'])
    .on("change", function(path, stats) {
      var directory = path.split( '/' ).slice( 0, -1 ).join( '/');
      var filename = path.split( '/' ).slice( -1 ).join( '/');
      /*
       * If we save a partial,
       * we should generate the closest files that don't have an underscore.
       */
      if(filename.startsWith("_")) {
        var componentDir = path.split( '/' ).slice( 0 , 2 ).join( '/');
        compileAllComponentSCSS(componentDir);
      } else {
        compileSassFile(path);
      }
    })
    .on("add", function(path, stats) {
      compileSassFile(path,stats);
    })
    .on("unlink", function(path, stats) {
      var directory = path.split( '/' ).slice( 0, -1 ).join( '/');
      var filename = path.split( '/' ).slice( -1 ).join( '/').split( '.' ).slice( 0, -1 );
      del([directory + '/dist/' + filename + '.css']);
      del([directory + '/dist/maps/' + filename + '.css.map']);
    });

  // If a twig is added, make sure to clear the cache
  gulp.watch(['components/**/*.html.twig'])
    .on('add', function(path, stats) {
      console.log('Adding ' + path + ' needs a cache rebuild!');
      rebuildDrupalCache(path);
    })
    .on('unlink', function(path, stats) {
      console.log('Removing ' + path + ' needs a cache rebuild!');
      rebuildDrupalCache(path);
    });

  // if something changes in the libraries folder, clear cache.
  gulp.watch(['*.libraries.yml'])
    .on('change', function(path, stats) {
      console.log('Changing the theme libraries needs a cache rebuild!');
      rebuildDrupalCache(path);
    });

  gulp.watch(['components/**/libraries.yml'])
    .on('change', function(path, stats) {
      console.log('Changing the library definition of ' + path + ' needs a cache rebuild!');
      rebuildDrupalCache(path);
    })
    .on('unlink', function(path, stats) {
      console.log('Removing the library definition of ' + path + ' needs a cache rebuild!');
      rebuildDrupalCache(path);
    })
    .on('add', function(path, stats) {
      console.log('Adding a new library definition: ' + path + ', needs a cache rebuild!');
      rebuildDrupalCache(path);
    });
});

gulp.task('default', gulp.series('watch'));
// Add build task for server-side
