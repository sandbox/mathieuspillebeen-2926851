<?php

/**
 * @file
 * Functions to support theming in the compony theme.
 */

use Drupal\Core\Serialization\Yaml;

/**
 * Implements hook_library_info_alter().
 */
function compony_library_info_alter(&$libraries, $extension) {
  // Alter the library of this theme only
  if ($extension == 'compony') {
    // Check which folders are under compony/components/
    $components = glob(drupal_get_path('theme', 'compony') . '/components/*', GLOB_ONLYDIR);

    // For every found foulder, check if there is a libraries.yml file.
    foreach ($components as $key => $component) {
      if (file_exists($component . '/libraries.yml')) {
        // If there is a yml file available within the component's folder, include it as a library.
        try {
          $new_libraries = Yaml::decode(file_get_contents($component . '/libraries.yml'));
          foreach ($new_libraries as $key => $new_library) {
            $libraries[$key] = $new_library;
          }
        } catch (InvalidDataTypeException $e) {
          // Rethrow a more helpful exception to provide context.
          throw new InvalidLibraryFileException(sprintf('Invalid library definition in %s: %s', $component . '/libraries.yml', $e->getMessage()), 0, $e);
        }
      }
    }
  }
}
